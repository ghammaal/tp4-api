/* On inclut l'interface publique */
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
typedef struct allocator_header allocator_header;
struct allocator_header {
        size_t memory_size;
	      mem_fit_function_t *fit;
        struct fb* first_block;
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}

typedef struct fb fb;
struct fb {
	size_t size;
	fb* next;
  int state;
};
/* si state == 1 la zone est libre
 * si state == 0 la zone est occupée
 */
void mem_init(void* mem, size_t taille)
{
  memory_addr = mem;
  *(size_t*)memory_addr = taille;
  // mem_fit_first est la fonction fit à utiliser
  mem_fit(&mem_fit_first);
  get_header()->memory_size = taille;
  /* On initialise la liste des zones libres avec une
	seule zone correspondant à l'ensemble de la mémoire
  située en mem et de taille taille non utilisée par
  votre allocateur pour son propre usage.*/
  get_header()->first_block = (fb*) (memory_addr + sizeof(allocator_header));
  get_header()->first_block->size= taille - sizeof(allocator_header) - sizeof(fb);
  get_header()->first_block->next=NULL;
  get_header()->first_block->state=1;

	/* On vérifie qu'on a bien enregistré les infos et qu'on
	 * sera capable de les récupérer par la suite
	 */
	assert(mem == get_system_memory_addr());
	assert(taille == get_system_memory_size());

}

void mem_show(void (*print)(void *, size_t, int)) {
fb* addr_fb = memory_addr + sizeof(allocator_header) ;
	while ((void*)addr_fb < get_system_memory_addr()+get_system_memory_size()) {
		print(addr_fb, addr_fb->size, addr_fb->state);
		addr_fb += sizeof(fb) + addr_fb->size;
	}
}

void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}

//retourne un pointeur vers la zone allouée de taille size
void *mem_alloc(size_t taille) {
	if(taille == 0){
		return NULL;
	}
	if(taille > get_system_memory_size() - sizeof(allocator_header) - sizeof(fb)){
		return NULL;
	}
	__attribute__((unused))/* juste pour que gcc compile ce squelette avec -Werror */
	fb* fb_res = get_header()->fit(get_header()->first_block, taille);

	return fb_res;
}


void mem_free(void* mem) {
	if(mem == NULL){
		return;
	}
	fb* fb_free = (fb*)mem;
	fb_free->state = 1;
	
	fb* prev_fb = NULL;

	fb* addr_fb = memory_addr + sizeof(allocator_header) ;

	if(addr_fb == fb_free){
		return;
	}

	while ((void*)addr_fb < get_system_memory_addr()+get_system_memory_size()) {
		if(addr_fb->state == 0){
		break;
		}
		prev_fb = addr_fb; 
		addr_fb += sizeof(fb) + addr_fb->size;
	}
	fb_free->next = prev_fb->next;
	prev_fb->next = fb_free;
}

fb* mem_fit_first(fb *list, size_t size) {
	fb* addr_fb = list;
	fb* addr_fb_prev = addr_fb;

	while (addr_fb != NULL) {

		if (addr_fb->state == 1 && addr_fb->size >= size) {
			addr_fb->state = 0;
			break;
		}
		addr_fb_prev = addr_fb;
		addr_fb = addr_fb->next;
	}
	if (addr_fb == NULL) {
		return NULL;
	}
	addr_fb_prev->next = addr_fb->next;
	return addr_fb;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return 0;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
fb* mem_fit_best(fb *list, size_t size) {
	fb* addr_fb = list;
	fb* best_fb = NULL;
	fb* best_fb_prev = NULL;
	fb* addr_fb_prev = addr_fb;

	while (addr_fb != NULL) {
		if (addr_fb->state == 1 && addr_fb->size >= size) {
			best_fb_prev = addr_fb_prev;
			if (best_fb == NULL) {
				best_fb = addr_fb;
			} else if (best_fb->size > addr_fb->size) {
				best_fb = addr_fb;
			}
		}
		addr_fb_prev = addr_fb;
		addr_fb = addr_fb->next;
	}

	if (best_fb == NULL) {
		return NULL;
	}

	best_fb_prev->next = best_fb->next;
	return best_fb;
}

fb* mem_fit_worst(fb *list, size_t size) {
	fb* addr_fb = list;
	fb* worst_fb = NULL;
	fb* worst_fb_prev = NULL;
	fb* addr_fb_prev = addr_fb;

	while (addr_fb != NULL) {
		if (addr_fb->state == 1 && addr_fb->size >= size) {
				worst_fb_prev = addr_fb_prev;
			if (worst_fb == NULL) {
				worst_fb = addr_fb;
			} else if (worst_fb->size < addr_fb->size) {
				worst_fb = addr_fb;
			}
		}
		addr_fb_prev = addr_fb;
		addr_fb = addr_fb->next;
	}

	if (worst_fb == NULL) {
		return NULL;
	}
	worst_fb_prev->next = worst_fb->next;
	return worst_fb;
}